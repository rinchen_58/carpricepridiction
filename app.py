from flask import Flask,render_template,request
import pickle
import pandas as pd

app = Flask(__name__)
model = pickle.load(open("car_model1.pkl","rb"))

cols = ['Make', 'Model', 'Year', 'Engine Fuel Type', 'Engine HP',
       'Engine Cylinders', 'Transmission Type', 'Driven_Wheels',
       'Number of Doors', 'Market Category', 'Vehicle Size', 'Vehicle Style',
       'highway MPG', 'city mpg', 'Popularity']

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/home")
def home():
    return render_template("home.html")

@app.route("/predict",methods=["POST"])
def output():
    if(request.method == "POST"):
        # print(request.form)
        Make = request.form['Make']
        print(Make)
        Model = request.form['Model']
        print(Model)
        Year = int(request.form['Year'])
        print(Year)
        EngineFuelType = request.form['EngineFuelType']
        print(EngineFuelType)
        EngineHP = float(request.form['EngineHP'])
        print(EngineHP)
        EngineCylinders = float(request.form['EngineCylinders'])
        print(EngineCylinders)
        TransmissionType = request.form['TransmissionType']
        print(TransmissionType)
        DrivenWheels = request.form['DrivenWheels']
        print(DrivenWheels)
        NumberOfDoors = float(request.form['NumberOfDoors'])
        print(NumberOfDoors)
        MarketCategory = request.form['MarketCategory']
        print(MarketCategory)
        VehicleSize = request.form['VehicleSize']
        print(VehicleSize)
        VehicleStyle = request.form['VehicleStyle']
        print(VehicleStyle)
        HighwayMPG = int(request.form['HighwayMPG'])
        print(HighwayMPG)
        CityMPG = int(request.form['CityMPG'])
        print(CityMPG)
        Popularity = int(request.form['Popularity'])
        print(Popularity)

        x_sample = [[Make, Model, Year, EngineFuelType, EngineHP,EngineCylinders, TransmissionType, DrivenWheels,NumberOfDoors, MarketCategory, VehicleSize, VehicleStyle,HighwayMPG, CityMPG, Popularity]]
        # demo = [["BMW","1 Series M",2011,"premium unleaded (required)",335.0,6.0,"MANUAL","rear wheel drive",2.0,"Factory Tuner,Luxury","High-Performance","Compact","Coupe",26,19,3919]]
        X = pd.DataFrame(x_sample,columns=cols)
        result = model.predict(X)
        result_str = ', '.join(map(str, result))
        print(result_str)

        return render_template("output.html",value=result)


if __name__=="__main__":
    app.run(debug=True)

